# DON'T USE THIS

Please. I beg you.

---

Server dotfiles.

## How to use

1. Install Fedora IoT as usual
2. Do some basic preparation for the other steps
	1. `hostnamectl hostname server.home.arpa`
	2. `sudo rpm-ostree install --apply-live ansible git fish wget rsync`
3. Put the contents of this repo into the home folder
	1. `cd ~`
	2. `rm -rf * .*`
	3. `git clone 'https://gitlab.com/warningnonpotablewater/server-dotfiles.git' .`
	4. `rm -rf .git README.md`
4. Change the shell to fish with `sudo usermod -s /usr/bin/fish $USER`
5. Run `,do-everything`
6. Reboot

## License

```
Copyright 2022 Warning: Non-Potable Water

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
```
