set nobackup
set nowritebackup
set noswapfile

set shada="NONE"

set guicursor=
set mouse=

set number

set tabpagemax=5353555

set tabstop=4
set shiftwidth=4

set list
set listchars=tab:-->,trail:█,nbsp:␣

set laststatus=2
set statusline=\ %F\ [%R%M]\ [%Y]\ [%{&fenc}]\ [%{&ff}]%=ln\ %l/%L\ (%p%%)\ ch\ %c\ 

highlight linenr ctermfg=red
highlight nontext ctermfg=darkgray
highlight statusline cterm=none ctermbg=gray ctermfg=black

inoremap <CR> <CR>x<BS>

function UseSpaces(number)
	setlocal expandtab
	setlocal listchars+=tab:██,trail:·
	
	exe "setlocal tabstop=" . a:number
	exe "setlocal shiftwidth=" . a:number
endfunction

filetype plugin indent off

autocmd filetype markdown setlocal textwidth=70
autocmd filetype yaml call UseSpaces(2)
autocmd filetype zig call UseSpaces(4)
