function ls --wraps=ls
	command ls -l\
		--all\
		--color\
		--group-directories-first\
		--time-style="+%F %T"\
		--si\
		$argv;
end
