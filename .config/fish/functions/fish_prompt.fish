function fish_prompt
	printf '%s%s%s@%s%s%s:%s%s%s$%s '\
		(set_color yellow) (whoami) (set_color normal)\
		(set_color green) (prompt_hostname) (set_color normal)\
		(set_color cyan) (prompt_pwd) (set_color purple)\
		(set_color normal)
end
